# Using Network_Analysis on QGIS 3

This project contains a tutorial (in french) explaining how to use the plugin Network_Analysis (developped by Serge Lhomme) on Qgis 3 in order to map scientific collaboration data between french cities.

* An Edgelist and a list of nodes are available in .csv
* A shapefile of the french regions is available in .zip
* The Network_Analysis plugin is available in .zip
* The tutorial is available in .pdf

More information on the data provided with this tutorial can be found on the website [http://geoscimo.univ-tlse2.fr/](http://geoscimo.univ-tlse2.fr/)

More information on the plugin can be found on the personnal website [http://serge.lhomme.pagesperso-orange.fr/index.html](http://serge.lhomme.pagesperso-orange.fr/index.html)

This tutorial can also be found on the Hypothese blog of the fmr group [https://groupefmr.hypotheses.org/4742](https://groupefmr.hypotheses.org/4742)